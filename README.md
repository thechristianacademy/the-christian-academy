Whether our students choose college, trade school, or the workforce, they can represent Christ in every area of society. While our school is predominantly college-preparatory, our students' time from Kindergarten to 12th grade helps them discern their gifts & abilities so they can pursue excellence.

Address: 4301 Chandler Drive, Brookhaven, PA 19015, USA

Phone: 610-872-5100

Website: https://www.tca-pa.org